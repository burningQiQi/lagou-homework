[TOC]
# 简答题

## 一、请简述 Vue 首次渲染的过程。

1. 在调试Vue初始化的过程中，首先是对Vue的静态成员以及原型成员进行初始化。

   1.1 可以看到首先进入的是 core/instance/index.js文件

   - 首先执行initMixin方法，结束之后在Vue的原型上添加了`_init`方法
   - 执行stateMixin方法，结束之后在Vue的原型上增加了`$data`，`$props`，`$delete`，`$set`，`$watch`方法
   - 执行eventsMixin方法，结束之后在Vue的原型上增加了`$emit`，`$off`，`$on`，`$once`方法
   - 执行lifecycleMixin方法，结束之后在Vue原型上增加了`$destroy`，`$forceUpdate`方法
   - 执行renderMixin方法，可以看到原型上挂载了很多下划线开头的单字母方法，当把模板转换成render函数，在render函数中要调用这些方法，上面还挂载了`$nextTick`和`_render`方法

   1.2 执行完毕之后又跳到了core/index.js的initGlobalAPI函数中

   - 里面给Vue添加了`config`属性并初始化了一些值
   - 给Vue原型添加了静态方法`delete`，`nextTick`，`observable`，`set`，`util`
   - 给Vue添加了options静态属性，然后将`component`，`directive`，`filter`，`_base`添加进去
   - 给options.component中添加组件`keepAlive`
   - 给Vue原型添加了静态方法`use`，`mixin`和`extend`
   - 给Vue原型添加了静态方法注册全局指令`Vue.directive()`，注册全局组件`Vue.component()`，注册全局过滤器`Vue.filter()`

   1.3 执行完毕之后又跳到了web/runtime/index.js中

   - 注册全局组件`Transition`和`TransitionGroup`，注册全局指令`model`和`show`
   - 给Vue原型上添加方法`__patch__`和`$mount`，这两个方法只是定义并没有调用，是在`init`方法中调用的

   1.4 执行完毕之后进入web/entry-runtime-with-compile.js中

   - 首先对`$mount`进行重写，添加了模板编译的功能，并给Vue添加了静态方法`compile`

2. 当成员定义好之后就是Vue的渲染过程，Vue构造函数内部首先其调用了`_init` 方法，这个方法里面，触发了beforeCreate和created钩子函数，还触发了$mount渲染整个页面

3. 进入entry-runtime-with-compile.js的$mount中，先将模板编译成render函数，然后进入web/runtime/index.js中的$mount方法

4. $mount方法中调用了mountComponent渲染DOM并挂载到DOM树上，

   - 4.1 里面先触发了钩子函数beforeMount
   - 4.2 然后定义了updateComponent方法，里面`vm._render()`的功能将render函数转化成虚拟DOM，`vm._update`的功能将虚拟DOM转化成真实DOM更新到页面上
   - 4.3 创建了Watcher对象，并把updateComponent传入，在Watcher中调用updateComponent，将render函数最终转化成真实DOM更新到界面上，并且注册了依赖，以后监听更新数据变化执行更新
   - 4.4 触发钩子函数mounted，界面渲染完毕



## 二、请简述 Vue 响应式原理。
答：Vue响应式指的是Vue数据响应式，Vue数据响应式指的是数据驱动视图，在数据发生变化的时候自动更新视图，不需要手动操作DOM。

源码中在数据初始化的时候initData中，是通过observe函数给数据data创建响应式对象的，这个函数的功能是通过创建一个Observer构造函数，将数据data的所有属性转化成getter和setter。

具体的做法：

1. 在Observer构造函数里面，添加了dep属性，还遍历所有的属性用defineReactive方法将其转换成setter和getter

2. 在defineReactive内部，调用了Object.defineProperty，在get方法中获取值并且去收集依赖，如果有子对象就给子对象收集依赖 ; 在set方法中，判断如果设置的新值与旧值不相等，将新值赋值给旧值，然后调用dep.notify方法派发通知。
3. 具体如何收集依赖?初始化的时候在$mount方法中调用了mountComponent方法，这个方法内部创建了Watcher对象，在Watcher对象中首先将自己存储到了Dep对象的target属性中，然后调用了updateComponent方法，这个方法将render函数渲染到页面上，渲染过程中在访问每个属性的时候，就会进入属性的get方法，在get方法中进行依赖收集，将Watcher对象添加到Dep的subs数组中，并且将dep对象添加到Watcher对象的newDeps数组中。
4. 具体如何发布通知?当值进行修改的时候，会触发属性的set方法，这个时候会调用dep.notify()方法，Dep对象会遍历其subs数组，每个元素都是一个Watcher对象，并调用其update方法，然后其会调用updateComponent方法更新视图。

## 二、请简述虚拟 DOM 中 Key 的作用和好处。
答：从源码中可以看出来，key主要有两处很重要的用处

1. 在重要判断两个节点是否相同的函数sameVnode函数的标准，一个是tag一个是key，如果两个都相同就会最大化的进行复用，这相比只有一个tag相同，更可以增加复用，减少DOM操作
2.  第二个是updateChildren中如果新旧开始和结束节点都不同的时候，要拿着新节点的key去老节点数组中寻找先相同的key的节点索引，如果没有key，只能依次去老节点数组中寻找，有key的话寻找效率更高



## 二、请简述 Vue 中模板编译的过程。

答：模板编译的主要目的是将template转换为渲染函数render

先放一个总结的图：

![模板编译过程](./images/vue-template.png)

下面详细讲解一下：

这个过程是在入口文件`entry-runtime-with-compile.js`中完成的，在这个文件中对`$mount`方法进行了重写，内部调用了`compileToFunctions`函数，传入了`template`模板和一些用户定义的`options`，这个函数执行完毕之后就返回了`render`函数和`staticRenderFns`静态根节点数组并将其挂载到`options`上。

所以从`compileToFunctions`出发，可以知道编译的过程。

1. 现在先认识一个函数baseCompile，这个函数的作用是接收template模板和合并之后的options，其内部进行以下处理，是核心函数
   - 第一：将把模板转换成 ast 抽象语法树对象
   - 第二：优化抽象语法树(标记静态根节点)
   - 第三：把 ast 抽象语法树对象生成字符串形式的js代码
2. 然后认识一下函数compile，这个函数的作用是将用户传入的options和平台相关的options进行合并，然后把template模板和合并后的options在调用上面的函数baseCompile的时候传入，之后得到字符串形式的js代码，添加一些错误和信息返回
3.  最后回到函数compileToFunctions，这个函数是由高阶函数createCompileToFunctionFn返回，里面接收template模板和用户传入的options
   - 一、先去缓存中查看是否有编译结果，如果有就直接返回，没有就下一步
   - 二、调用compile函数进行编译，完成得到字符串形式的js代码render和静态跟节点js代码的数组：{ render, staticRenderFns }
   - 三、那字符串形式的js代码通过createFunction函数，转换成js函数，同样静态根节点数组中的每一个元素也转换成js函数
   - 四、缓存对象并返回

总结一下就是，用户把template和options传入，compile函数内部将options进行合并，调用baseCompile将模板转换成字符串形式的js代码，并标记好静态根节点、静态节点之类的信息，之后调用createFunction函数将js字符串转换成render函数的js代码返回，模板编译结束。

## 笔记

这次偷懒没有转pdf，直接放链接

- [响应式原理.md](http://note.youdao.com/noteshare?id=ff86e74348b3e1ac9e8764626af3da75&sub=646B19F5C1E64BA881EE21637B23CABA)
- [虚拟DOM.md](http://note.youdao.com/noteshare?id=da2f2f0e885f8a9bfb59c7231d474c08&sub=8E2930CC09DE447BADE0165A966D2DE2)
- [模板编译.md](http://note.youdao.com/noteshare?id=a7da2fac88ca699657e22c4507a4362c&sub=4870E4581513498FA51E2652EDB4027F)
- [组件化.md](http://note.youdao.com/noteshare?id=3150ef54b297a398d8479fdffdb46c33&sub=974EEA1C9C034C86A78A90F818EA050A)

