[TOC]
# 实践题

## 使用 NuxtJS 实现 RealWorld 全部功能
- DEMO地址   https://demo.realworld.io/#/

要求

1. 首先到 demo 地址注册一个自己的账号, 方便自己代码调测

2. 实现功能包括: 注册/登录/退出、文章发表/修改/列表/分页/标签/详情/点赞/取消点赞、个人中心，个人信息更新

3. 本作业需要 放在 github 个人仓库, 并且实现自动化部署

4. 作业提交 「仓库代码地址」，在 README.md 中 输出「部署成功的服务器地址」

说明

作业所需服务器, 可以到下面地址购买,  0.1元使用3个月 (需要拉勾学员电话号码注册)

https://www.ucloud.cn/site/active/lagou.html

## 完成

[GitHub地址](https://github.com/a1burning/nuxt-hu77Home-realWorld)

访问网址：http://59.110.231.59/nuxt-hu77Home-realWorld/

本地项目地址：SSR/07-nuxt-hu77-realworld/

## 笔记

这次偷懒没有转pdf，直接放链接

- [Vuex状态管理.md](http://note.youdao.com/noteshare?id=5a00eb630f0f11ecf86301150c2f8c53&sub=5C0A2A9C2A944E14ADE3D4587F908FEA)
- [购物车案例.md](http://note.youdao.com/noteshare?id=d1deb0e857d6230e3e468e4e5ee226c6&sub=E5C600CED1F9474E97D41DA52C2AE67F)
- [简单模拟实现一个Vuex.md](http://note.youdao.com/noteshare?id=5dad468ec7039e9b3c416fd80f94d38e&sub=221485A298884002BF24122A795983A9)
- [服务端渲染基础.md](http://note.youdao.com/noteshare?id=df71878ef2d9a5bd13999bb1dcdc0389&sub=C77BF6F702AE46D9A95C86B8F042C5A6)
- [NuxtJS基础](http://note.youdao.com/noteshare?id=6887a71fccba6e2ac17be859b590f2c9&sub=F51A6D89E9BA4C74B6249F721D7FEC80)
- [Nuxt综合案例](http://note.youdao.com/noteshare?id=50cc21d13a52eacd18732209747745e1&sub=74185CEFE6CA4FDFA0772CFADC0CC69A)


