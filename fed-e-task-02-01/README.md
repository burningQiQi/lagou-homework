[TOC]
# 简答题

## 一、谈谈你对工程化的初步认识，结合你之前遇到过的问题说出三个以上工程化能够解决问题或者带来的价值？

答：工程化是一种思维方式，尽可能将人工把控的部分通过机器程序把控，这样可以减少人工的不稳定因素，减少出错。我们在开发阶段，更简便的开发语言（sass，less，es6，ts）出现，更好的开发思维（模块化、模板引擎）都可以让我们的工作更加的简便，如果时候这些必定要经过编译，压缩和转化。

看上去比较麻烦，但是如果把这么编译、压缩、转化都看成车间，那么不管形形色色的代码进去，都可以统一包装，高效快捷。一键压缩，一键转化，一键部署，都让我们避免了代码提交，人工转化，人工上线带来的不稳定性，无疑对公司还是员工，都是一个很好的事情。

之前一直使用FIS进行构建，所以拿这个作为例子：
1. 首先是一键发布，命令很简单`fis3 release -d output`，在FIS的解决方案中，还有针对PHP为后端的解决方案，FIS可以发布到本地服务器上，还可以一键发布到后端服务器上（前提是后端服务器要配置文件），这样在联调阶段，不需要改动一次手动部署，而且速度很快，利用`-w`就可以边改边看效果。
2. 其次是资源定位，项目中用相对定位的方式，还可以加md5戳，这样发布到机器上的时候，自动转化为加md5戳的绝对路径，可以避免服务器缓存造成的长时间没有更新页面的问题，也可以避免静态资源加载路径的问题。
3. 关于工程化里面模板解析`fis3-smarty`、less编译等都能够转化，当然除了babel= =，只需要一个`fis-conf.js`，很多东西都可以搞定。
4. 还有是按需加载的思维，项目中很多简单的压缩功能，发布功能已经集成，需要的可以去插件库下载，保证了FIS运行高效，速度快。不能满足条件的插件，可以自己写一个FIS插件，但是因为我的能力和时间有限，并没有深入研究这里。

## 二、你认为脚手架除了为我们创建项目结构，还有什么更深的意义？

答：查了一些资料。我觉得脚手架大部分是用来创建项目结构的，但是其不仅仅是一个结构，里面还包含了，使用哪些技术栈、使用哪些编程思维，用到了哪些前端解决方案的一个集成体。例如我们用vue-cli，就是使用vue框架，里面用less，用typescript开发等，包括了本地调试和生成生产项目。当前如果一个公司的前端团队，再进行进一步封装的话，如果项目结构大体相同，其实可以一键生成一个项目（理想情况）。除了那些大的，小到一个通用组件，其实也可以使用脚手架，随用随安装，还可以增加一些程序员的趣味性。

# 编程题
## 一、概述脚手架实现过程，并使用NodeJS完成一个自定义的小型脚手架工具
1. 本地创建一个有目录结构规范的文件夹，并创建一个gitHub仓库，并远程连接到本地
2. 安装yoman-generator基类，并在index.js里面写好核心内容
3. npm link到全局在别的项目中启用
4. 本地测试没有问题远程提交代码，并npm publish，脚手架就完成了

脚手架地址 https://www.npmjs.com/package/generator-csfdemo

## 二、尝试使用 Gulp 完成项目的自动化构建

说明文档在这里：

### 自动化构建说明文档

1. 首先安装`npm i gulp --save-dev`，在gulpfile.js中引用，并且将输入流src，输出流dest，组合任务之并行任务，组合任务之串行任务，监听文件这几个方法解构出来
```js
const {src, dest, parallel, series, watch } = require('gulp')
```

2. 其次明确我们这个构建任务的几个模块
- 一个是生产环境需要的任务build，一个是开发环境需要的任务develop
- 开发环境需要的任务是，编译sass，es6和模板引擎，还需要本地服务器及热更新
- 生产环境需要的任务是，编译sass，es6和模板引擎，将图片文字进行压缩，将第三方引用模块打包，输出的所有文件进行压缩，最后生成到dist输出目录
- 生产环境打包之前需要将之前生成的目录清除掉

3. 下面将任务结构屡清楚
```js
// 实现这个项目的构建任务
const {src, dest, parallel, series, watch } = require('gulp')

// 配置
const data = {}
// 清除任务
const clean = () => {
}
// 基本编译任务
// sass
const style = () => {
  return src()
  .pipe()
}
//es6
const script = () => {
  return src()
  .pipe()
}
//templ
const page = () => {
  return src()
  .pipe()
}
// 图片压缩
const image = () => {
  return src()
  .pipe()
}
// 文字压缩
const font = () => {
  return src()
  .pipe()
}
// 其他文件
const extra = () => {
  return src()
  .pipe()
}

//创建服务任务 
const serve = () => {
}

// 对第三方引用文件进行打包
const useref = () => {
  return src()
  .pipe()
}
// 生产环境build任务
const build
// 开发环境develop任务
const develop

module.exports = {
}
```

4. 我们先对配置选项，进行一下配置
```js
const data = {
  // 文件目录，根据现有文件夹路径
  build: {
    // 输入目录
    src: 'src',
    // 输出目录
    dest: 'dist',
    // 临时文件目录，因为编译之后会有一个目录，之后要进行代码压缩，这个时候在同一个目录中读写会有问题，所以需要将编译之后的代码输入到一个临时文件夹中
    // 使用范围： 
    // 1. 首先sass,es6,templ的编译输出目录都是temp
    // 2. 服务器的监听目录也是temp，因为编译之后就可以在浏览器中运行，不需要太繁琐的操作
    // 3. image和font输出目录是dist，最后的生产目录，开发环境压不压缩没有关系
    // 4. 第三方生成vender的目录，是从临时目录中取的
    temp: 'temp',
    // styles里面匹配了所有的scss文件
    styles: 'assets/styles/*.scss',
    scripts: 'assets/scripts/*.js',
    pages: '**/*.html',
    images: 'assets/images/**',
    fonts: 'assets/fonts/**',
    public: 'public'
  },
  // 端口号可配置
  serve: {
    port: '3000'
  },
  // 模板引擎需要参数，从老师那里拷贝
  data: {
    menus: [
    // ...
    ],
    pkg: require('./package.json'),
    date: new Date()
  }
}
```

5. 安装`npm i del --save-dev`清除模块并引用，对清除任务进行修改
```js
// 引用del模块
const del = require('del')

const clean = () => {
  // 参数是数组，清除目录，一个是输出的目录，还有一个是临时文件的目录
  return del([data.build.dest, data.build.temp])
}
```

6. 安装`npm i gulp-load-plugins --save-dev`用于更好的管理`gulp-`模块并使用
```js
// 安装的gulp-后面的插件会显示在这里，gulp-clean-css这种的会用驼峰命名，使用的时候plugins.cleanCss()
const plugins = require('gulp-load-plugins')()
console.log(plugins)
```

7. 安装`npm i gulp-sass --save-dev`用于编译sass到css，因为上面用了load-glugins进行管理，这里不用在require引用了，直接修改

```js
const style = () => {
  // 首先使用src，第一个参数是路径，第二个参数base是基于src路径下面的目录结构，输出的时候保持一致，cwd是按照那个目录为基准下去匹配对应的文件
  // 
  return src(data.build.styles, {base: data.build.src, cwd: data.build.src})
    // 使用sass进行编译，输出风格是完全展开，如果不这么处理，最后一个花括号不会折行
    .pipe(plugins.sass({ outputStyle: 'expanded'}))
    // 输出到临时文件夹中
    .pipe(dest(data.build.temp))
}
```

8. 安装`npm i gulp-babel @babel/core @babel/preset-env --save-dev`用于编译es6，光安装gulp-babel是不行的，然后要进行修改

```js
const script = () => {
  // 输入目录还是文件夹下面所有的js文件，基准目录是src，从src下面查找
  return src(data.build.scripts, { base: data.build.src, cwd: data.build.src})
    // 下面输出的流用babel进行处理，使用的环境要引用那么模块
    .pipe(plugins.babel({
      presets: ['@babel/preset-env']
    }))
    // 输出到临时文件夹中
    .pipe(dest(data.build.temp))
}
```

9. 安装`npm i gulp-swig --save-dev`用于编译模板引擎，然后修改任务

```js
const page = () => {
  // 输出目录是所有的html文件
  return src(data.build.pages, { base: data.build.src, cwd: data.build.src})
  // 使用swig方法并且将之前定义的模板的数据穿进去
  .pipe(plugins.swig({
    data: data.data
  }))
  // 输出到临时文件夹中
  .pipe(dest(data.build.temp))
}
```
10. 上面之后所有的编译任务完成，接下来是对图片和文字的压缩，安装`cnpm i gulp-imagemin --save-dev`，然后修改任务
```js
const image = () => {
  // 找到路径下所有的图片文件
  return src(data.build.images, { base: data.build.src, cwd: data.build.src})
  // 进行压缩处理
  .pipe(plugins.imagemin())
  // 这里不用输出到临时文件夹中，因为开发环境的时候不需要压缩，直接输出到最终目录即可
  .pipe(dest(data.build.dest))
}

const font = () => {
  return src(data.build.fonts, { base: data.build.src, cwd: data.build.src})
  // 也要进行压缩处理
  .pipe(plugins.imagemin())
  // 也要输出到最终目录
  .pipe(dest(data.build.dest))
}
```

11. 将所有的其他任务进行处理

```js
const extra = () => {
  // public目录下的所有文件，就是静态文件，直接输出到最终目录
  return src('**', { base: data.build.public, cwd: data.build.public})
  .pipe(dest(data.build.dest))
}
```

12. 创建本地服务并且可以实现热更新，安装`npm i browser-sync --save-dev`，并且在项目中引用

```js
// 引用并创建
const bs = require('browser-sync').create()

const serve = () => {
  // 实现服务初始化并且进行配置
  return bs.init({
    // 把提示关掉
    notify: false,
    // 服务端口号
    port:data.serve.port,
    // 核心配置
    server: {
      // 网站根目录，因为我们这个服务是编译之后生成的临时目录中直接发布的，所以根目录就是临时文件夹，如果文件路径找不到，会依次到下面的目录中寻找，serve任务没有输出dist文件，所以这里不到dist文件中找。
      baseDir: [data.build.temp, data.build.src, data.build.public],
      // 在引用第三方文件的时候，这里的绝对路径引用会报错，所以这个先于baseDir，有这个匹配的地址先去相对路径的node_modules中寻找
      routes:{
        '/node_modules': 'node_modules'
      }
    }
  })
}
```
那我们需要热更新，修改sass，js，html文件的时候要刷新浏览器，如果sass，js，html有修改要进行编译，图片文字修改之后也要刷新浏览器。需要用到watch方法进行监听
```js
const style = () => {
  return src(...)
    // 这单个任务下面，如果当前任务执行，需要重新刷新浏览器，以流的方式发送到浏览器中
    .pipe(bs.reload({ stream: true }))
}

const script = () => {
  return src(...)
    .pipe(bs.reload({ stream: true }))
}

const page = () => {
  return src(...)
  // 特别说明：这边因为有模板引擎的缓存，所以修改html不会及时看到刷新，所以这里要用cache把缓存去掉。
  .pipe(plugins.swig({
    dafault: { cache: false},
    data: data.data
  }))
  .pipe(...)
  .pipe(bs.reload({ stream: true }))
}

const serve = () => {
  // 监听如果sass文件有修改，就执行sytle任务，并且刷新浏览器（这步在任务中）
  watch(data.build.styles, { cwd: data.build.src }, style)
  // 监听如果js文件有修改，就执行script任务，并且刷新浏览器（这步在任务中）
  watch(data.build.scripts, { cwd: data.build.src }, script)
  // 监听如果html文件有修改，就执行page任务，并且刷新浏览器（这步在任务中）
  watch(data.build.pages, { cwd: data.build.src }, page)
  // 监听如果图片和文字有修改，直接刷新浏览器，不需要压缩图片和文字
  watch([
    data.build.images,
    data.build.fonts
  ],{
    cwd: data.build.src
  }, bs.reload)

  // 监听如果其他文件夹中有修改，直接刷新浏览器
  watch('**', { cwd: data.build.public }, bs.reload)

  return bs.init({
    notify: false,
    port:data.serve.port,
    server: {
      baseDir: [data.build.temp, data.build.src, data.build.public],
      routes:{
        '/node_modules': 'node_modules'
      }
    }
  })
}
```
接下来将开发的develop任务补充完整
```js
// 编译任务可以同时，都完成之后，再进行服务器发布
const develop = series(parallel(style, script, page), serve)
```

13. 开发环境任务中有第三方引入插件，很多的话需要打包成vendor，这里用到了`npm i gulp-useref --save-dev`，这个插件可以读取注释，根据注释将对应的引入打包成一个（未压缩过的）。

类似于这种的：
```html
<!-- build:css assets/styles/vendor.css -->
  <link rel="stylesheet" href="/node_modules/bootstrap/dist/css/bootstrap.css">
  <!-- endbuild -->
  <!-- build:css assets/styles/main.css -->
  <link rel="stylesheet" href="assets/styles/main.css">
  <!-- endbuild -->
```

那么我们对第三方的插件任务进行修改
```js
const useref = () => {
  // 匹配所有的html文件
  return src('**/*.html', { base: data.build.temp , cwd: data.build.temp })
  // 从temp目录中进行查找，并且进行打包
  .pipe(plugins.useref({ searchPath: ['temp', '.']}))
  // 这里输出到最终的目录，不能输出到本目录，因为读写有问题，所以也是为啥要用临时目录的原因
  .pipe(dest(data.build.dest))
}
```
这样打包出来了vender.css等，但是都没有压缩代码，所以在输出到最终目录的时候需要进行压缩操作

14. 压缩生成的css,js,html文件，并且因为需要匹配不同的文件做不同的压缩操作，这里还要对文件进行区分
- `npm i gulp-if --save-dev` 文件区分模块
- `npm i gulp-htmlmin gulp-uglify gulp-clean-css --save-dev` 分别是html，js，css压缩模块，对useref任务进行修改

```js
const useref = () => {
  return src('**/*.html', { base: data.build.temp , cwd: data.build.temp })
  .pipe(plugins.useref({ searchPath: ['temp', '.']}))
  // 匹配html文件进行压缩
  .pipe(plugins.if(
    /\.html$/,plugins.htmlmin({
      // 深度配置选项，删除空白字符，删除内联css和js的格式
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: true
    })
  ))
  // 匹配css文件进行压缩
  .pipe(plugins.if(
    /\.css$/,plugins.cleanCss()
  ))
  // 匹配js文件进行压缩
  .pipe(plugins.if(
    /\.js$/,plugins.uglify()
  ))
  .pipe(dest(data.build.dest))
}
```

14. 对生产环境的build任务进行编写

思路：
- 先执行原来文件的清除工作
- 图片文字和编译工作并不冲突，可以并行
- 编译之后才能进行第三方打包并且压缩代码

```js
const build = series(clean, parallel(series(parallel(style, script, page), useref),image, font, extra))
```

其中develop中对编译有复用的代码，可以进行提取

```js
// 编译任务
const trans = parallel(style, script, page)
// 生产环境build任务
const build = series(clean, parallel(series(trans, useref),image, font, extra))
// 开发环境develop任务
const develop = series(trans, serve)

//将需要暴露的任务暴露出来
module.exports = {
  clean,
  build,
  develop
}
```

15. 本地调试`gulp develop`运行没有问题，`gulp build`的时候报错`gulp-imagemin找不到模块`，重新进行安装`cnpm i gulp-imagemin --save-dev`之后再运行`gulp build`，顺利完成。输出目录均没有问题。

16. 自动化构建完毕