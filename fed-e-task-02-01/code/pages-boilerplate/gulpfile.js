// 实现这个项目的构建任务
const {src, dest, parallel, series, watch } = require('gulp')

const plugins = require('gulp-load-plugins')()
const del = require('del')

const bs = require('browser-sync').create()

// 配置
const data = {
  build: {
    src: 'src',
    dest: 'dist',
    temp: 'temp',
    styles: 'assets/styles/*.scss',
    scripts: 'assets/scripts/*.js',
    pages: '**/*.html',
    images: 'assets/images/**',
    fonts: 'assets/fonts/**',
    public: 'public'
  },
  serve: {
    port: '3000'
  },
  data: {
    menus: [
      {
        name: 'Home',
        icon: 'aperture',
        link: 'index.html'
      },
      {
        name: 'Features',
        link: 'features.html'
      },
      {
        name: 'About',
        link: 'about.html'
      },
      {
        name: 'Contact',
        link: '#',
        children: [
          {
            name: 'Twitter',
            link: 'https://twitter.com/w_zce'
          },
          {
            name: 'About',
            link: 'https://weibo.com/zceme'
          },
          {
            name: 'divider'
          },
          {
            name: 'About',
            link: 'https://github.com/zce'
          }
        ]
      }
    ],
    pkg: require('./package.json'),
    date: new Date()
  }
}

// 清除任务
const clean = () => {
  return del([data.build.dest, data.build.temp])
}
// 基本编译任务
const style = () => {
  return src(data.build.styles, {base: data.build.src, cwd: data.build.src})
    .pipe(plugins.sass({ outputStyle: 'expanded'}))
    .pipe(dest(data.build.temp))
    .pipe(bs.reload({ stream: true }))
}

const script = () => {
  return src(data.build.scripts, { base: data.build.src, cwd: data.build.src})
    .pipe(plugins.babel({
      presets: ['@babel/preset-env']
    }))
    .pipe(dest(data.build.temp))
    .pipe(bs.reload({ stream: true }))
}

const page = () => {
  return src(data.build.pages, { base: data.build.src, cwd: data.build.src})
  .pipe(plugins.swig({
    dafault: { cache: false},
    data: data.data
  }))
  .pipe(dest(data.build.temp))
  .pipe(bs.reload({ stream: true }))
}

const image = () => {
  return src(data.build.images, { base: data.build.src, cwd: data.build.src})
  .pipe(plugins.imagemin())
  .pipe(dest(data.build.dest))
}

const font = () => {
  return src(data.build.fonts, { base: data.build.src, cwd: data.build.src})
  .pipe(plugins.imagemin())
  .pipe(dest(data.build.dest))
}

const extra = () => {
  return src('**', { base: data.build.public, cwd: data.build.public})
  .pipe(dest(data.build.dest))
}

//创建服务任务 
const serve = () => {
  watch(data.build.styles, { cwd: data.build.src }, style)
  watch(data.build.scripts, { cwd: data.build.src }, script)
  watch(data.build.pages, { cwd: data.build.src }, page)
  watch([
    data.build.images,
    data.build.fonts
  ],{
    cwd: data.build.src
  }, bs.reload)

  watch('**', { cwd: data.build.public }, bs.reload)

  return bs.init({
    notify: false,
    port:data.serve.port,
    server: {
      baseDir: [data.build.temp, data.build.src, data.build.public],
      routes:{
        '/node_modules': 'node_modules'
      }
    }
  })
}

// 对编译出来的内容进行别的操作，比如压缩
const useref = () => {
  return src('**/*.html', { base: data.build.temp , cwd: data.build.temp })
  .pipe(plugins.useref({ searchPath: ['temp', '.']}))
  .pipe(plugins.if(
    /\.html$/,plugins.htmlmin({
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: true
    })
  ))
  .pipe(plugins.if(
    /\.css$/,plugins.cleanCss()
  ))
  .pipe(plugins.if(
    /\.js$/,plugins.uglify()
  ))
  .pipe(dest(data.build.dest))
}

const trans = parallel(style, script, page)
// 生产环境build任务
const build = series(clean, parallel(series(trans, useref),image, font, extra))
// 开发环境develop任务
const develop = series(trans, serve)
module.exports = {
  clean,
  build,
  develop
}