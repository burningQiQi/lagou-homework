[TOC]
## 简答题

### 1、Vue 3.0 性能提升主要是通过哪几方面体现的？

答： 主要从三个方面提现：响应式系统的升级、编译优化和源码体积优化。

#### 响应式系统的升级

1. 主要使用了proxy对象重写了响应式系统，proxy的性能本身比defineProperty要好，还可以对代理对象进行拦截属性的访问、赋值和删除操作。
2. 不需要在初始化的时候遍历所有的属性，在vue2.x中，初始化的时候要对data进行遍历，如果data的属性是对象，要递归处理每一个对象的属性。而在Vue3.0中，如果有嵌套对象的时候，只有在访问某个属性的时候才会去递归处理，性能提升。
3. 使用proxy对象，可以默认监听动态添加的属性、还可以监听删除的属性、数组的索引和length属性，这些在Vue2.x中无法实现或者需要调用额外函数

#### 编译优化

1. Vue3.0中为了提升性能重写了虚拟DOM
2. **静态提升** —— 在编译的时候，Vue3中通过标记静态节点以及patchFlag标记动态节点，diff的时候只需要对比动态节点的内容，而2.x中diff的过程中，只会跳过静态根节点，静态节点还需要进行diff。
3. **引入Fragments片段特性** —— 2.x中，必须留一个唯一的根节点，这个限制在Vue3中不再存在，因为3.0中新引入了Fragments，就算没有唯一的根节点，其也会生成一个片段维持整体的树形结构。
4. **新增缓存事件处理函数** —— 缓存的函数绑定之后不会发生变化，但是运行这个函数的时候，会获取最新的handler，这避免了不必要的更新。
5. **按需引入** —— 需要的模块例如transition，用的时候可以再引入，减少了不必要的引用。

#### 源码体积优化

1. **优化源码** —— Vue3.0中移除了一些不常用的API：inline-template、filter等，可以让最终的体积变小。
2. **更好的支持Tree-shaing** —— 除了Vue的核心模块，其他模块没有引入是不会打包进去的，还有不同的构建版本，根据需要使用。

### 2、Vue 3.0 所采用的 Composition Api 与 Vue 2.x使用的Options Api 有什么区别？

答：Vue3.0的Composition Api在设计之初，是为了解决使用2.x开发长期迭代和维护的大型项目时遇到超大组件，使用options API不好拆分和重用的问题。

一个简单的options Api需要有data、methods、生命周期钩子等内容，如果一个功能同时涉及这几个选项，代码就会比较零散地拆分到不同的选项中，开发的时候来回拖动，还可能要更改多个地方。这种API还难以提取组件中可重用的逻辑。

而使用Composition Api，主要是以功能划分，一个函数表示一个功能，里面包含了需要的响应式数据、方法和生命周期函数中需要的操作，这种方式可以更灵活的组织代码结构，还可以将函数进行重用。

### 3、Proxy 相对于 Object.defineProperty 有哪些优点？

答：
1. proxy是专门为对象设置代理器的，那么proxy就可以轻松监视到对象的读写过程。相比较Object.defineProperty，proxy的功能更加强大，使用起来也更为方便，Object.defineProperty只能监视属性的读写，proxy能够监视到更多对象的操作，例如删除属性操作。
2. proxy可以更高的支持数组对象的监视，Object.defineProperty不能实现对数组的索引和length属性的监听
3. 使用proxy可以默认监听动态添加的属性，而Object.defineProperty不能。
4. Proxy是以非侵入的方式监管了对象的读写，其不需要对对象本身进行操作就能监视到成员的读写。而Object.defineProperty需要在对象本身上进行操作。

### 4、Vue 3.0 在编译方面有哪些优化？
答：
1. **静态提升** —— 在编译的时候，会将静态节点提升到最上面，这些静态节点只有在初始化的时候才会被创建一次，当再次调用render的时候不需要再创建这些节点，可以直接重用上一次创建静态节点对应的VNode。
2. **patchFlag** —— Vue3中通过标记静态节点以及patchFlag标记动态节点，diff的时候只需要对比动态节点的内容，而2.x中diff的过程中，只会跳过静态根节点，静态节点还需要进行diff。
2. **引入Fragments片段特性** —— 2.x中，必须留一个唯一的根节点，这个限制在Vue3中不再存在，因为3.0中新引入了Fragments，就算没有唯一的根节点，其也会生成一个片段维持整体的树形结构。
3. **新增缓存事件处理函数** —— 缓存的函数绑定之后不会发生变化，但是运行这个函数的时候，会获取最新的handler，这避免了不必要的更新。
4. **按需引入** —— 需要的模块例如transition，用的时候可以再引入，减少了不必要的引用。


### 5、Vue.js 3.0 响应式系统的实现原理？
答：Vue.js3.0底层采用proxy对象实现属性监听，初始化的时候不需要遍历所有的属性通过defineProperty转化成getter和setter，如果有多层属性嵌套，在访问属性过程中处理下一级属性。

先说一下收集依赖整个过程：

> 收集依赖要提到effect函数，定义effect的时候首次会执行一次这个函数，只要修改里面的响应式数据，函数就会再次调用。

- 使用track函数收集依赖：定义目标对象字典targetMap，通过目标对象key找到其对应的字典value(也就是depsMap)，depsMap存储的key是目标对象的属性名称，value是对应的effect函数的集合dep，在调用effect函数的时候，访问了某个响应式属性，就会调用track函数，将effect添加到对象属性的dep集合中(会去重)

然后说一下触发更新的过程：

- 使用trigger函数触发更新：如果有触发更新的操作，那么就执行这个函数，通过目标和属性去dep集合中，找到所有的effect函数并调用。


最后说一下几个将数据转化成响应式数据的几个函数：

- reactive函数：只能把对象转化成响应式对象，返回一个proxy对象，设置其get函数，访问并调用track函数收集依赖，set函数设置新值并调用trigger函数触发更新，删除函数也是删除值并调用trigger函数触发更新。
- ref函数：如果是普通对象就用reactive创建并返回，如果是原始值，就创建有value属性的对象，并添加getter和setter返回，在getter中调用track函数收集依赖，在setter中调用trigger函数触发更新。
- toRefs函数：因为reactive对象重新赋值或者解构都会丢失响应式，所以这里的toRefs可以解决这个问题，接收reactive返回的proxy对象，然后将传入的所有属性都转换成ref函数返回的对象。



## 笔记

这次偷懒没有转pdf，直接放链接

文档：1. Vue3.0介绍.md
链接：http://note.youdao.com/noteshare?id=6ad91e576f77776a61c05fc860e33bc9&sub=978C4E7F7FE0477B92BA8EC9657C68A1

文档：2. Composition API.md
链接：http://note.youdao.com/noteshare?id=94f570943f04a62f81e037487f9095a8&sub=A3EA25A406B24F9A8641C0DCBE61D948

文档：3. Vue.js3.0响应式系统原理.md
链接：http://note.youdao.com/noteshare?id=85126ccb60147a26eadcfb2c8e9e4e30&sub=391AA396799D4D2CB3E784C4B7939C52

文档：4. Vite实现原理.md
链接：http://note.youdao.com/noteshare?id=917e669ffbb24e4ab768223cac6ceaaf&sub=BB1C9D8A9C63428D988E2182E309E657
