[TOC]
# 实践题

## 使用 Gridsome 实现 blog 全部功能
- DEMO地址   https://gridsome-forestry.netlify.app/

要求：开发并部署

## 完成

[GitHub地址](https://github.com/a1burning/hu77-forestry-blog)

访问网址：http://hu77gridsome.hu77.top/

本地项目地址：Gridsome/3-4-homework-blog-forestry/

相关Strapi: http://strapiblog.hu77.top/admin

## 笔记

这次偷懒没有转pdf，直接放链接

文档：1. 搭建自己的SSR.md
链接：http://note.youdao.com/noteshare?id=23e9853e4ae2190abc8910a11c0e1f7a&sub=CD89501121064DC38A9369796BB986CE

文档：2. 静态站点生成.md
链接：http://note.youdao.com/noteshare?id=e19442e68c3b1dd4b4977756969460fb&sub=F25BF01DE29C46379FF557FD40138B98

文档：3. 下载包出错的解决方式.md
链接：http://note.youdao.com/noteshare?id=779acf78d4e107914c07a6de18de969f&sub=48A4E473E7044CD19A4F7D9230FBA703

文档：4. Gridsome案例.md
链接：http://note.youdao.com/noteshare?id=63f73f47f7fe97ac04d0bf1c20109e43&sub=05EB302A6BC047899E53DC58CCC38D05

文档：5. 组件化开发.md
链接：http://note.youdao.com/noteshare?id=409328c33231a1125cbec6d0775229e8&sub=6F32F1156FCA4AE88EB835BBF9DEB5AB

文档：8. 作业.md
链接：http://note.youdao.com/noteshare?id=1e5d3a1c274f782e681739a01998063e&sub=597F1174AF6C4DA9BA20AFEF9ABE6D54