class Observer {
  //传入data并且在walk中调用
  constructor(data) {
    this.walk(data)
  }

  // 遍历对象的属性
  walk (data) {
    // 1. 判断data是否是对象
    if (!data || typeof data !== 'object') {
      return 
    }
    // 2. 遍历data对象的所有属性
    Object.keys(data).forEach(key => {
      this.defineReactive(data, key, data[key])
    })
  }
  // 这里的obj是vue.js中的this.$data属性，外部对get方法有引用，get方法又用到了value，所以此处发生了闭包，闭包没有把value释放掉
  defineReactive(obj, key, value) {
    let self = this
    let dep = new Dep()
    // value如果是字符串内部不会做任何处理，如果是对象，也会把对象内部的数据转换成响应式数据
    this.walk(value)
    // 直接调用转换
    Object.defineProperty(obj, key, {
      enumerable: true,
      configurable: true,
      // 这里用value不用obj[key]是因为每次这么写，obj[key]，就会调用get方法，这样get方法出现了循环访问，所以这里直接用value
      get () {
        // 收集依赖：这里需要结合Watcher对象看，Dep.target如果存在这里面存放的就是观察者对象
        Dep.target && dep.addSub(Dep.target)
        return value
      },
      set (newValue) {
        if(value === newValue) {
          return 
        }
        value = newValue
        // 判断newValue是不是对象，如果不是就直接返回，如果是对象，给每一个属性添加getter/setter
        self.walk(newValue)
        // 发送通知
        // TODO
        dep.notify()
      }
    })
  }
}