class Vue {
  constructor (options) {
    // 1. 通过属性保存选项的数据
    this.$options = options || {}
    this.$data = options.data || {}
    this.$methods = options.methods || {}
    // 如果是字符串说明是选择器，要转换成DOM对象，如果是对象就可以直接使用。
    this.$el = typeof options.el === 'string' ? document.querySelector(options.el) : options.el
    // 2. 把data中的成员转换成getter和setter，注入到vue实例中
    this._proxyData(this.$data)
    this._proxyData(this.$methods)
    // 3. 调用observer对象，监听数据的变化
    new Observer(this.$data)
    // 4. 调用compiler对象，解析指令和差值表达式
    // 需要传递vue实例，直接传递this即可
    new Compiler(this)
  }
  // 让Vue代理data中的属性
  _proxyData (data) {
    // 遍历data中的所有属性
    Object.keys(data).forEach(key => {
      // 把data的属性注入到vue的实例中
      Object.defineProperty(this, key, {
        //是否可枚举可修改
        enumerable: true,
        configurable: true,
        get () {
          return data[key]
        },
        set (newValue) {
          if(newValue === data[key]) {
            return 
          }
          data[key] = newValue
        }
      })
    })
    
  }
}
