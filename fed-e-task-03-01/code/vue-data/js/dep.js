class Dep {
  constructor () {
    // 存储所有的观察者
    this.subs = []
  }
  // 添加观察者
  addSub (sub) {
    // 判断参数是否是观察者：是否为空，是不是有update方法
    if(sub && sub.update) {
      this.subs.push(sub)
    }
  }
  // 发送通知
  notify () {
    // 遍历所有的观察者并调用其update方法
    this.subs.forEach(sub => {
      sub.update()
    })
  }
}