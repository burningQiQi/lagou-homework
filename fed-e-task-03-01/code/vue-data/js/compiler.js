class Compiler {
  constructor (vm) {
    this.el = vm.$el
    this.vm = vm
    this.compile(this.el)
  }
  // 编译模板，处理文本节点和元素节点
  compile(el) {
    // 第一层子节点
    let childNodes = el.childNodes
    Array.from(childNodes).forEach(node => {
      if (this.isTextNode(node)) this.compileText(node)
      if (this.isElementNode(node)) this.compileElement(node)

      // 判断当前节点是否有子节点存在，存在就递归调用compile
      if (node.childNodes && node.childNodes.length) {
        this.compile(node)
      }
    })
  }

  // 编译元素节点，处理指令
  compileElement(node) {
    // 只模拟v-text、v-model，这些是属性, node.attributes可以获取属性节点，是个伪数组
    // 需要知道属性名称，名称对应功能，名称关联的数据要展示到指令指定的位置处
    /**
     * name是属性名称 v-text
     * value是属性值 msg
     */
    Array.from(node.attributes).forEach(attr => {
      let attrName = attr.name
      // 判断属性是否是指令
      if(this.isDirective(attrName)) {
        // v-text => text，去掉前面的v-
        attrName = attrName.substr(2)
        // 获取属性值
        let key = attr.value
        this.update(node, key, attrName)
      }
    })
  }
  
  /**
   * 调用指令对应的方法
   * @param {*} node 当前node节点
   * @param {*} key data属性的名字 msg or count
   * @param {*} attrName text or model，即方法前缀
   */
  update (node, key, attrName) {
    // 指令对应的方法
    let updateFn
    if (attrName.startsWith('on')) {
      console.log(attrName)
      const attrNameArr = attrName.split(':')
      console.log(attrNameArr)
      updateFn = this[attrNameArr[0] + 'Updater']
      updateFn && updateFn.call(this, node, this.vm[key], key, attrNameArr[1])
    }else{
      updateFn = this[attrName + 'Updater']
      updateFn && updateFn.call(this, node, this.vm[key], key)
    }
    // 判断是否有值，有值就调用，this.vm[key]可以获取属性key的值

    // 这里添加watcher的时候进行了一次修改，将修改updateFn中的this指向，this指的是Compiler的实例
  }

  /**
   * 处理v-text指令，值取出来赋值给对应的dom元素
   * @param {*} node node节点
   * @param {*} value 值
   */  
  textUpdater (node, value, key) {
    node.textContent = value
    // 添加key
    // 这里的this指的是Compiler实例，前面改变过this的指向
    new Watcher(this.vm, key, (newValue) => {
      node.textContent = newValue
    })
  }

  // 处理v-model指令
  modelUpdater (node, value, key) {
    // 表单元素更新的是value属性
    node.value = value
    // 添加key
    new Watcher(this.vm, key, (newValue) => {
      node.value = newValue
    })
    // 双向绑定
    node.addEventListener('input',() => {
      // 获取当前表单的值node.value
      // 赋值给key对应的数据 this.vm[key]
      this.vm[key] = node.value
    })
  }

  /**
   * 处理v-html指令
   * @param {*} node 
   * @param {*} value 
   * @param {*} key 
   */
  htmlUpdater (node, value, key) {
    node.innerHTML = value
    new Watcher(this.vm, key, (newValue) => {
      node.innerHTML = newValue
    })
  }

  /**
   * 处理v-on指令，冒号后面跟事件名字，等号后面跟函数
   * @param {*} node 
   * @param {*} value 
   * @param {*} key 
   */
  onUpdater (node, value, key, type) {
    node['on' + type] = () => {
      this.vm[key] && this.vm[key].call(this.vm)
    }
  }

  // 编译文本节点，处理插值表达式
  compileText(node) {
    // console.dir是把后面的参数以对象的形式打印出来，判断textcontent内容即可
    // console.dir(node)
    /**
     * 1. 匹配形式
     * 1.1 {}在正则中有含义，所以加\进行转义
     * 1.2 中间可能有很多空格，所以用.匹配任意字符
     * 1.3 +是指.匹配的字符可以出现一次或者多次
     * 1.4 ?是非贪婪模式，尽早结束匹配
     * 1.5 ()括号有分组含义，用于之后的提取方便
     * 1.6 全局匹配使用g，满足一个文本节点中有多个插值表达式的情况
     */
    let reg = /\{\{(.+?)\}\}/
    // 获取当前节点的内容 {{   msg }}
    let value = node.textContent
    // 匹配如果是插值表达式
    if(reg.test(value)) {
      // 将变量提取，RegExp.$1指的是第一个分组的内容，第二个分组的内容是$2，处理后的结果将空格进行处理
      let key = RegExp.$1.trim()
      // 将值进行替换， 文本节点原来内容的插值表达式替换成变量对应的值 {{ msg }} => hello vue
      // 将value进行reg规则匹配，得到的结果用this.vm[key]进行替换
      /**
       * replace方法：
       *  参数一：规定子字符串或要替换的模式的 RegExp 对象。
       *  参数二：变量对应的值，比方msg对应的值，key是msg的变量名，通过这个属性获取对应的值
       */
      node.textContent = value.replace(reg, this.vm[key])

      // 创建Watcher对象并更新对象
      new Watcher(this.vm, key, (newValue) => {
        node.textContent = newValue
      })
    }
  }
  // 判断是否是指令 v-开头
  isDirective (attrName) {
    return attrName.startsWith('v-')
  }
  //判断节点是否是文本节点
  isTextNode (node) {
    // nodeType是类型 1是元素节点 2是属性节点 3是文本节点
    return node.nodeType === 3
  }

  //判断节点是否是元素节点
  isElementNode(node) {
    return node.nodeType === 1
  }
}