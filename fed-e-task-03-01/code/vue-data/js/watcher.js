class Watcher {
  constructor (vm, key, cb) {
    this.vm = vm
    // data中的属性名称
    this.key = key
    //回调函数，更新视图
    this.cb = cb
    // 当前watcher对象记录到Dep类的静态属性target中
    Dep.target = this
    // 触发get方法，在get方法中会调用addSub
    // 将旧值赋值给属性，这里直接访问了对象，触发了get方法
    this.oldValue = vm[key]
    // 添加到数组之后置为空，防止多次添加
    Dep.target = null
  }

  /**
   * 1. 当数据发生变化的时候更新视图
   */
  update() {
    let newValue = this.vm[this.key]
    // 如果旧数据和新数据一样，不做处理
    if(this.oldValue === newValue) return
    // 不一样的时候更新视图
    this.cb(newValue)
  }
}