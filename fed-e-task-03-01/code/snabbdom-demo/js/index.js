import { h } from 'snabbdom/build/package/h'
import { init } from 'snabbdom/build/package/init'
import { styleModule } from 'snabbdom/build/package/modules/style'
import { eventListenersModule } from 'snabbdom/build/package/modules/eventlisteners'

let vnode
let data
let patch = init([
  styleModule,
  eventListenersModule
])

// 初始数据函数
function initData() {
  let data = []
  for (let i = 0 ; i < 10 ; i++) {
    data.push({
      rank:i+1,
      title: '标题'+ (i+1),
      content: '内容' + (i+1),
      index: Math.floor(Math.random() * 100)
    })
  }
  return data
}

// 全部内容渲染
function dataView (data) {
  let vnode = h('div#container', {
    style: {
      color: '#333',
      position: 'relative'
    }
  }, [
    h('div', {
      style: {
        position: 'absolute',
        top:'20px',
        right: '20px',
        color: 'blue',
        padding: '10px',
        border: '1px solid blue'
      },
      on: {
        click: addData
      }
    },'Add'),
    h('h1','新闻新闻大新闻'),
    h('ul', {
      style: {
        overflow: 'hidden'
      }
    },[
      h('li.nav', {
        on: {
          click: [sort, 'rank']
        }
      }, 'rank排序'),
      h('li.nav', {
        on: {
          click: [sort, 'index']
        }
      }, 'index排序')
    ]),
    h('ul', data.map(listView))
  ])
  return vnode
}

// 列表渲染
function listView (data) {
  return h('li.list', {
    key:data.rank,
    style: {
      borderBottom: '1px solid gray',
      overflow: 'hidden',
      padding: '10px 0'
    }
  }, [
    h('h3.left.pr20', data.title),
    h('p.left.pr20', data.content),
    h('p.left.pr20', 'index='+data.index),
    h('div.right', {
      on: {
        click: [deleteData, data]
      }
    }, 'delete')
  ])
}

// 添加数据
function addData () {
  const single = {
    rank:data.length + 1,
    title: '标题'+ (data.length + 1),
    content: '内容' + (data.length + 1),
    index: Math.floor(Math.random() * 100)
  }
  data.unshift(single)
  render()
}

// 排序数据
function sort (e, type) {
  type = type.data.on.click[1]
  console.log(type)
  data.sort((a, b) => {
    if(a[type] > b[type]) {
      return 1
    }
    if(a[type] < b[type]) {
      return -1
    }
    return 0
  })
  render()
}

// 删除数据
function deleteData (e, single) {
  single = single.data.on.click[1]
  data = data.filter((s) => {
    return s !== single
  })
  render()
}

// 更新视图
function render () {
  vnode = patch(vnode, dataView(data))
}

// 初始数据
data = initData()
console.log(data)

// load监听渲染页面
window.onload = () => {
  let app = document.querySelector('#app')
  vnode = patch(app, dataView(data))
}

