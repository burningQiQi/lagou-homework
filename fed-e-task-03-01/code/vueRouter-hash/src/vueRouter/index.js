let _Vue = null
export default class VueRouter {
  // 实现Vue的插件机制
  static install (Vue) {
    // 如果已经注册过了不再注册
    if (VueRouter.install.installed) {
      return
    }
    VueRouter.install.installed = true
    // 将Vue对象传入
    _Vue = Vue
    // 将options.router对象混入到vue实例上
    _Vue.mixin({
      beforeCreate () {
        // 判断vue的$options上有么有router属性
        if (this.$options.router) {
          // 如果有就给vue的原型对象输入$router对象
          Vue.prototype.$router = this.$options.router
          this.$options.router.init()
        }
      }
    })
  }

  constructor (options) {
    // 创建类的时候传入的参数
    this.options = options
    // 记录路由和组件的对应关系
    this.routeMap = {}
    // 创建响应式对象
    this.data = _Vue.observable({
      // 存放当前路由地址，/ 是根目录
      current: '/'
    })
  }

  /**
   * 初始化
   */
  init () {
    this.createRouteMap()
    this.initComponents(_Vue)
    this.initEvent()
  }

  /**
   * 记录路由和组件的对应关系
   */
  createRouteMap () {
    // 遍历所有路由规则
    this.options.routes.forEach(route => {
      this.routeMap[route.path] = route.component
    })
  }

  /**
   * 初始化组件
   * @param {ObjectConstructor} Vue Vue构造函数
   */
  initComponents (Vue) {
    // 创建router-link组件
    Vue.component('router-link', {
      props: {
        to: String
      },
      render (h) {
        return h('a', {
          attrs: {
            href: '#' + this.to
          }
        }, [
          this.$slots.default
        ])
      }
    })
    const self = this
    // 创建router-view组件，用于渲染组件
    Vue.component('router-view', {
      render (h) {
        // 加载对应的组件，如果组件不存在默认加载*
        let component = self.routeMap[self.data.current]
        if (!component) {
          component = self.routeMap['*']
        }
        return h(component)
      }
    })
  }

  /**
   * 初始化事件，给onhashchange注册事件，如果hash有变化更改当前路由
   */
  initEvent () {
    window.onhashchange = (e) => {
      const hash = window.location.hash
      this.data.current = hash.slice(1) || '/'
    }
  }
}
