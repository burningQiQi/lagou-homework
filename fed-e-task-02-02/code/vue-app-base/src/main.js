import Vue from 'vue'
// import App from './App.vue'

import './style.less'
Vue.config.productionTip = false

import(/* webpackChunkName: 'App' */'./App.vue').then(({ default: App }) => {
  new Vue({
    render: h => h(App)
  }).$mount('#app')
})

if (module.hot) {
  module.hot.accept('./assets/logo.png', () => {
    console.log('App.vue 模块更新了，这里需要手动处理模块')
  })
}
