const common = require('./webpack.common')
const { merge } = require('webpack-merge')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin')


module.exports = merge(common, {
  output: {
    filename: '[name]-[contenthash:8].bundle.js'
  },
  mode: 'production',
  devtool: 'none',
  plugins: [
    new CopyWebpackPlugin({
      patterns: [{
        from: 'public'
      }]
    }),
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: "[name]-[contenthash:8].bundle.css"
    }),
    new OptimizeCssAssetsWebpackPlugin()
  ]
})