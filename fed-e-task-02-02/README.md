[TOC]
# 简答题

## 一、Webpack 的构建流程主要有哪些环节？如果可以请尽可能详尽的描述 Webpack 打包的整个过程。

答：

webpack构建流程有以下几个环节

1. 根据配置文件找到入口文件

> 从webpack.config.js中找到入口文件

2. 解析资源模块，构建资源关系树

> 从入口文件入手根据import或者require等引入模块语句解析出整个项目文件依赖的模块，从而形成一个资源依赖关系树。

3. 对应资源文件进行loader加载器转换加载

> 递归遍历关系树，根据配置文件中的rules属性正则匹配对应的加载器，例如js文件匹配使用babel-loader，css文件匹配使用css-loader和style-loader等，将其对应的代码转成js

4. 将结果输出到输出文件

> 将加载器加载好的对应的模块合并到指定的bundle.js中，形成打包好的文件



## 二、Loader 和 Plugin 有哪些不同？请描述一下开发 Loader 和 Plugin 的思路。
答：

1. 原理不同

- loader是资源加载器，对于不同的资源文件（如css、img文件等进行加载）使得其能正确打包到bundle.js中，这个是打包流程的核心。
- plugin比loader拥有更广泛的功能，它作用于webpack的生命钩子中挂载功能，让我们可以在不同的环节扩展功能（例如输出文件之前先清空目录，输出之后自动生成一个html文件等等），而且因为有了plugins，可以让webpack实现更多等可以优化开发和生产流程的高级特性（自动编译、HMR、Source Map、打包分包等）。

2. 作用范围不同

- loader是在生成资源依赖树和向输出文件中合并中起作用。
- plugin作用域wbepack的所有生命钩子中挂载功能实现扩展。

3. 配置的地方和方式不同

- loader安装之后在webpack.config.js中rules属性后面配置对象，使用哪个loader配置那些内容
- plugin安装之后在webpack.config.js中先用require导入，然后再plugins属性后面new一个实例对象使用

# 编程题
## 一、使用 Webpack 实现 Vue 项目打包任务

> 具体任务及说明：
> 
> 先下载任务的基础代码  百度网盘链接: https://pan.baidu.com/s/1pJl4k5KgyhD2xo8FZIms8Q 提取码: zrdd
> 
> 这是一个使用 Vue CLI 创建出来的 Vue 项目基础结构
> 
> 有所不同的是这里我移除掉了 vue-cli-service（包含 webpack 等工具的黑盒工具）
> 
> 这里的要求就是直接使用 webpack 以及你所了解的周边工具、Loader、Plugin 还原这个项目的打包任务
> 
> 尽可能的使用上所有你了解到的功能和特性

### 解题步骤：作业在code文件里

#### 一、先让项目跑起来
观察代码中有以下几种文件，首先要使用`npm run build`进行正常打包

1. package.json中配置script中的build为webpack

```js
"scripts": {
    "build": "webpack",
  }
```
2. 然后安装基本依赖`npm i webpack webpack-cli --save-dev`
3. 安装解析less加载器`npm i less less-loader css-loader style-loader --save-dev`，只安装less-loader会报错，查找资料还要安装less，之后css-loader和style-loader也要使用。
4. 先创建一个基本的webapck.config.js，对less加载器进行配置，从下往上顺序不能乱less加载器就好了。

```js
const path = require("path")
module.exports = {
  entry: './src/main.js',
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, 'dist')
  },
  mode: 'none',
  module: {
    rules: [{
      // 匹配到less进行加载器配置
      test: /\.less$/,
      use: [
        'style-loader',
        'css-loader',
        'less-loader'
      ]  
    }]
  }
}
```
4. 安装解析vue文件的加载器`npm i vue vue-loader --save-dev`，安装完成之后还需要额外安装一个插件解析模板`npm i vue-template-compiler --save-dev`安装完成之后进行配置，中间还需要引用一个插件`VueLoaderPlugin`，vue加载器完成

```js
const path = require("path")
// 必须要进行插件引用
const VueLoaderPlugin = require('vue-loader/lib/plugin')
module.exports = {
  entry: './src/main.js',
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, 'dist')
  },
  mode: 'none',
  module: {
    // 匹配到vue文件配置加载器
    rules: [{
      test: /\.vue$/,
      use: 'vue-loader'
    },{
      test: /\.less$/,
      use: [
        'style-loader',
        'css-loader',
        'less-loader'
      ]  
    },
    // 对vue内部的样式进行加载器解析
    {
      test: /\.css$/,
      use: [
        'style-loader',
        'css-loader',
    ]}]
  },
  plugins: [
    // 使用插件
    new VueLoaderPlugin()
  ]
}
```
5. 对图片文件安装加载器`npm i file-loader --save-dev`并进行配置

```js
{
  test: /\.png$/,
  use: 'file-loader'
},{

}
```

6. 现在运行`npm run build`项目可以正常跑起来了。

#### 二、添加ES6转义并可以在html中显示正常

1. 安装`babel-loader`对ES6代码进行编译`npm i babel-loader @babel/core @babel/preset-env --save-dev`，运行后发现错误,将`babel.config.js`删除之后进行配置

```js
{
  test: /\.js/,
  use: {
    loader: 'babel-loader',
    options: {
      presets: ['@babel/preset-env']
    }
  }
}
```

2. 安装`html-webpack-plugin`插件，这个插件自动生成html文件，并进行配置。需要把public中的html文件修改为.ejs，html文件无法解析里面的模板，`BASE_URL`没办法解析所以添加`DefinePlugin`插件，将`BASE_URL`定义为当前文件路径。

```js
const webpack = require("webpack")
const HtmlWebpackPlugin = require('html-webpack-plugin')
...
plugins: [
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      title: 'vue-webpack-sample',
      template: './public/index.ejs'
    }),
    new webpack.DefinePlugin({
      BASE_URL: '"./"'
    })
  ]
```

3. 安装`copy-webpack-plugin`将`public`文件拷贝到输出目录中去，并进行配置。

```js
const CopyWebpackPlugin = require('copy-webpack-plugin')
...
plugins: [
    new CopyWebpackPlugin({
      patterns: [{
        from: 'public'
      }]
    })
  ]
```

4. 生成了`index.html`打开看到img的src显示`[object Module]`，新版本默认将`esModule`置为true了，修改`file-loader`的配置为false如下，图片可以正常显示。

```js
{
  test: /\.png$/,
  use: {
    loader: 'file-loader',
    options: {
      esModule: false
    }
  }
}
```

#### 三、实现自动编译和浏览器热更新

1. 安装`npm i webpack-dev-server --save-dev`实现自动编译和浏览器更新，在`package.json`中配置script

```js
"scripts": {
    "serve": "webpack-dev-server --open",
    "build": "webpack"
  }
```
担心资源加载不到在`webpack.config.js`中配置
```js
devServer: {
  contentBase: './public'
}
```
之后运行`npm run serve`，可以看到浏览器自动开启，修改代码可以做到刷新页面。

#### 四、配置Source Map和HMR模块热替换

1. 配置`Source Map`要在`webpack.config.js`中配置

```js
devtool: 'cheap-eval-module-source-map'
```

2. `devserve`中配置`hotOnly`，如果处理`HMR`的代码报错，也不采用自动刷新的形式，这个时候修改css热替换成功。

```js
devServer: {
  contentBase: './public',
  hotOnly: true
},
```

3. 配置图片`logo.png`的热替换，安装模块`HotModuleReplacementPlugin`，并在`webpack.config.js`中引用

```js
plugins: [
  new webpack.HotModuleReplacementPlugin()
  ]
```

在`main.js`中添加代码，`logo.png`可以实现热替换。

```js
// 避免module没有hot对象报错
if(module.hot) {
  module.hot.accept('./assets/logo.png', () => {
    console.log('App.vue 模块更新了，这里需要手动处理模块')
  })
}
```


#### 五、分环境生成配置文件

1. 配置分环境，将`webpack.config.js`改名为`webpack.common.js`，并添加两个文件`webpack.dev.js`用于开发环境配置，`webpack.prod.js`用于生产环境配置。

2. `webpack.dev.js`和`webpack.prod.js`需要安装`webpack-merge`插件，合并`common`的配置。

```js
const common = require('./webpack.common')
const { merge } = require('webpack-merge')
module.exports = merge(common, {
  ...
})
```

3. 并对代码进行环境拆分

- 公共模块：入口文件、出口文件、`mode`默认为`none`，所有的`module`，`plugins`插件留下（`VueLoaderPlugin`解析vue文件，`HtmlWebpackPlugin`生成html文件，`DefinePlugin`定义BASE_URL常量）
- 开发模块：`mode`为`development`，`source map`的`devtool`设置为`cheap-eval-module-source-map`，`devServer`的所有内容，plugins插件留下（`HotModuleReplacementPlugin`）
- 生产模块：`mode`为`production`，`source map`的`devtool`设置为`none`，`plugins`插件留下（`CopyWebpackPlugin`）

```js
// webpack.common.js
const path = require("path")
const webpack = require("webpack")
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')


module.exports = {
  entry: './src/main.js',
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, 'dist')
  },
  mode: 'none',
  module: {
    rules: [{
      test: /\.vue$/,
      use: 'vue-loader'
    },{
      test: /\.less$/,
      use: [
        'style-loader',
        'css-loader',
        'less-loader'
      ]  
    },
    {
      test: /\.css$/,
      use: [
        'style-loader',
        'css-loader'
    ]},{
      test: /\.png$/,
      use: {
        loader: 'file-loader',
        options: {
          esModule: false
        }
      }
    },{
      test: /\.js$/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      }
    }
  ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      title: 'vue-webpack-sample',
      template: './public/index.ejs'
    }),
    new webpack.DefinePlugin({
      BASE_URL: '"./"'
    })
  ]
}

// webpack.dev.js
const common = require('./webpack.common')
const webpack = require("webpack")
const { merge } = require('webpack-merge')

module.exports = merge(common, {
  mode: 'development',
  devtool: 'cheap-eval-module-source-map',
  devServer: {
    contentBase: './public',
    hotOnly: true
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ]
})


// webpack.prod.js
const common = require('./webpack.common')
const { merge } = require('webpack-merge')
const CopyWebpackPlugin = require('copy-webpack-plugin')


module.exports = merge(common, {
  mode: 'production',
  devtool: 'none',
  plugins: [
    new CopyWebpackPlugin({
      patterns: [{
        from: 'public'
      }]
    })
  ]
})
```

4. 这个时候要配置`package.json`文件，并分别运行命令可以使用。

```js
"scripts": {
  // 要给每个环境命令配置对应的配置文件--config
  "serve": "webpack-dev-server --config webpack.dev.js --open",
  "build": "webpack --config webpack.prod.js"
}
```

#### 六、生产环境打包前清除目录

1. 安装`clean-webapck-plugin`插件，并在`webpack.prod.js`环境配置

```js
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
...
plugins: [
    ...
    new CleanWebpackPlugin()
  ]
```

#### 七、代码分包
1. 对css代码进行分包，安装`mini-css-extract-plugin`将css文件进行打包，安装`optimize-css-assets-webpack-plugin`插件将css文件进行压缩，并进行配置

- `webpack.common.js`: 这里将css分包用link引入代替style内联标签，修改之前的`style-loader`为`MiniCssExtractPlugin.loader`

```js
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
  ...
  module: {
    rules: [
      ...
    {
      test: /\.less$/,
      use: [
        MiniCssExtractPlugin.loader,
        'css-loader',
        'less-loader'
      ]  
    },
    {
      test: /\.css$/,
      use: [
        MiniCssExtractPlugin.loader,
        'css-loader'
    ]}
    ...
  ]
  }
}
```

- `webpack.dev.js: `开发环境需要引入css分包，但是不需要压缩代码。

```js
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = merge(common, {
  ...
  plugins: [
    ...
    new MiniCssExtractPlugin()
  ]
})
```

- `webpack.prod.js： `生产环境需要引入css分包，并且压缩代码

```js
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin')


module.exports = merge(common, {
  ...
  plugins: [
    ...
    new MiniCssExtractPlugin(),
    new OptimizeCssAssetsWebpackPlugin()
  ]
})
```

2. 对`App.vue`进行动态导入的方式分包，先写`main.js`

```js
// main.js
import Vue from 'vue'
// import App from './App.vue'

import './style.less'
Vue.config.productionTip = false

import(/* webpackChunkName: 'App' */'./App.vue').then(({ default: App }) => {
  new Vue({
    render: h => h(App),
  }).$mount('#app')
})
```

3. 对`webpack.common.js`进行修改，将入口文件和出口文件修改成分包形式。

```js
module.exports = {
  // 修改为键值的形式，输出的文件名进行动态配置
  entry: {
    index: './src/main.js'
  },
  output: {
    filename: '[name].bundle.js',
    path: path.join(__dirname, 'dist')
  }
  ...
}
```

这样执行`npm run build`可以看到分包成功。

#### 八、给生产环境下生成的文件添加hash

1. 在生产环境下给js和css都添加8位hash

```js
module.exports = merge(common, {
  output: {
    filename: '[name]-[contenthash:8].bundle.js'
  },
  ...
  plugins: [
    ...
    new MiniCssExtractPlugin({
      filename: "[name]-[contenthash:8].bundle.css"
    })
  ]
})
```

#### 九、给项目添加lint

1. 安装`eslint，eslint-loader`模块，并对elsint进行初始化

```bash
npx eslint --init


√ How would you like to use ESLint? · style       
√ What type of modules does your project use? · esm
√ Which framework does your project use? · vue
√ Does your project use TypeScript? · No
√ Where does your code run? · browser
√ How would you like to define a style for your project? · guide
√ Which style guide do you want to follow? · standard
√ What format do you want your config file to be in? · JavaScript
Checking peerDependencies of eslint-config-standard@latest
The config that you've selected requires the following dependencies:

eslint-plugin-vue@latest eslint-config-standard@latest eslint@^7.12.1 eslint-plugin-import@^2.22.1 eslint-plugin-node@^11.1.0 eslint-plugin-promise@^4.2.1
√ Would you like to install them now with npm? · Yes
```

2. 在`package.json`中进行配置，之后`eslint`可以正常工作

```js
// 匹配js文件，不包括node_modules文件，在babel-loader前面使用eslint-loader检测
{
  test: /\.js$/, 
  exclude: /node_modules/, 
  use: 'eslint-loader',
  enforce: 'pre'
}
```



