/**
 * 三、基于下面提供的代码,完成后续的四个练习
 * 练习一：
 * 练习1:使用fp.add(x, y)和fp.map(f,x)创建一个能让functor里的值增加的函数ex1
 * 
 */

const fp= require('lodash/fp')
const { Maybe, Container } = require('./support.js')

/**
 * 练习一：
 * 使用fp.add(x, y)和fp.map(f,x)创建一个能让functor里的值增加的函数ex1
 */

let maybe = Maybe.of([5, 6, 1])
// 创建一个函数，输入增加的值
let ex1 = (num) => {
  return maybe.map(value => fp.map(fp.add(num), value))
}
console.log(ex1(1)._value) //[ 6, 7, 2 ]


/**
 * 练习二:
 * 实现一个函数ex2,能够使用fp.first 获取列表的第一个元素
 */
let xs = Container.of(['do', 'ray','me', 'fa', 'so', 'Ia', 'ti', 'do'])
let ex2 = () => {
  return xs.map(fp.first)
}
console.log(ex2()._value) // do

/**
 * 练习三：
 * 实现一个函数ex3,使用safeProp和fp.first找到user的名字的首字母
 */

let safeProp = fp.curry(function (x, o) {
  return Maybe.of(o[x])
})
let user = { id: 2, name: 'Albert' }
let ex3 = (obj) => {
  return safeProp('name', obj).map(fp.first)
}

console.log(ex3(user)._value) // A

/**
 * 
 * 练习4:使用Maybe重写ex4,不要有if语句
 * 
 * let ex4 = function (n) {
    if (n) {
      return parseInt(n)
    }
  }
 */

 let ex4 = (n) => {
   return Maybe.of(n).map(parseInt)
 }

 console.log(ex4(null))
 console.log(ex4(2.3))



