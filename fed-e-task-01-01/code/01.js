/**
 * 一、将下面异步代码使用Promise的方式改进
 *  setTimeout(function () {
      var a = 'hello'
      setTimeout(function () {
        var b = 'lagou'
        setTimeout(function () {
          var c = 'I love U'
          console.log(a + b + c)
        }, 10)
      }, 10)
    }, 10)
 */

// 定义一个函数，返回promise对象，resolve里面返回传入的字符串参数
let p = (str) => {
  return new Promise((resolve, reject) => {
    resolve(str)
  })
}

p('hello')
.then(value => value +'lagou')
.then(value => value + 'I Love U')
.then(console.log)
