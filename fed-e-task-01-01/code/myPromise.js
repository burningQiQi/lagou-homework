// 首先定义几个常数变量,便于重复调用和提示
const PENDING = 'pending'
const FULFILLED = 'fulfilled'
const REJECTED = 'rejected'

// 定义一个类
class MyPromise {
  constructor (exector) {
    //传入执行器，立即执行，判断报错，将成功失败函数返回，使用的时候是调用
    try{
      exector(this.resolve, this.reject)
    } catch(e) {
      //执行器中有抛错，直接调用返回错误
      this.reject(e)
    }
  }

// 原型对象属性
  //状态，初始为pending
  status = PENDING
  //成功失败的值，初始为undefined
  value = undefined
  reason = undefined
  //成功失败回调数组，初始为空数组
  successCallback = []
  failCallback = []

  // 原型对象方法
  // 成功回调函数——修改状态、改变值
  resolve(value) {
    // 判断是不是等待状态
    if(this.status !== PENDING) return
    this.status = FULFILLED
    this.value = value
    while(this.successCallback.length) this.successCallback.shift()()
  }

  // 失败回调函数——修改状态、改变值
  reject(reason) {
    //判断是不是等待状态
    if(this.status !== PENDING) return
    this.status = REJECTED
    this.reason = reason
    while(this.failCallback.length) this.failCallback.shift()()
  }

  // 执行返回的promise对象结果
  // 输入回调函数，如果没有回调函数默认输出对应函数
  then(successCallback = value => value, failCallback = reason => {throw reason}) {
    // then返回的是一个promise对象
    let promise2 = new Promise((resolve, reject) => {
      // 判断promise执行的状态
      // 如果是成功状态
      if(this.status === FULFILLED){
        setTimeout(() => {
          try{
            //成功回调一个x
            let x = successCallback(this.value)
            // 包装调用返回正确的值
            resolvePromise(promise2, x, resolve, reject)
          }catch(e) {
            reject(e)
          }
        }, 0)
      // 如果是错误状态
      }else if(this.status === REJECTED) {
        setTimeout(() => {
          try{
            //成功回调一个x
            let x = failCallback(this.reason)
            // 包装调用返回正确的值
            resolvePromise(promise2, x, resolve, reject)
          }catch(e) {
            reject(e)
          }
        }, 0)
      // 如果是等待状态——异步状态
      // 将所有then的调用都push到回调数组中
      }else{
        this.successCallback.push(() => {
          setTimeout(() => {
            try{
              //成功回调一个x
              let x = successCallback(this.value)
              // 包装调用返回正确的值
              resolvePromise(promise2, x, resolve, reject)
            }catch(e) {
              reject(e)
            }
          }, 0)
        })
        this.failCallback.push(() =>{
          setTimeout(() => {
            try{
              //成功回调一个x
              let x = failCallback(this.reason)
              // 包装调用返回正确的值
              resolvePromise(promise2, x, resolve, reject)
            }catch(e) {
              reject(e)
            }
          }, 0)
        })
      }
    })
    return promise2
  }

  // 只返回错误的情况
  catch(failCallback) {
    return this.then(undefined, failCallback)
  }

  finally(callback) {
    // 无论成功和失败都执行
    return this.then(value => {
      return MyPromise.resolve(callback()).then(() => value)
    },reason =>{
      return MyPromise.resolve(callback()).then(() => {throw reason})
    })
  }

  // 构造函数方法
  // 如果是promise对象直接返回，如果不是就新建一个promise对象返回
  // 只返回成功状态
  static resolve (value) {
    if(value instanceof MyPromise) return value
    return new MyPromise(resolve => resolve(value))
  }

  // 传递的是一个数组
  static all (array) {
    // 结果保存
    let result = []
    // 计数器
    let sum = 0
    return new MyPromise((resolve, reject) => {
      // 把数据添加到结果数组中
      let addData = (key,value) => {
        result[key] = value
        // 计数，如果等于数组长度，就整体返回
        sum ++
        if(sum === array.length) {
          resolve(result)
        }
      }
      // 对数组进行遍历，如果元素是promise对象，那么调用，如果有一个是错误的就直接返回
      // 如果是其他元素，直接添加到数组中
      for (let i = 0; i < array.length; i++) {
        let current = array[i]
        if (current instanceof MyPromise) {
          current.then(value => addData(i, current), reason => reject(reason))
        } else {
          addData(i, current)
        }
      }
    })
  }

}

// 处理函数
// 传自己、返回的promise对象、成功回调、失败回调
function resolvePromise(promise2, x, resolve, reject) {
  // 如果传自己就会报错
  if(promise2 === x){
    return reject(new TypeError('不能循环返回自己'))
  }
  // 判断x是不是promise对象，如果是就调用返回，如果不是就包装一个promise对象返回
  if(x instanceof MyPromise){
    x.then(resolve, reject)
  } else {
    resolve(x)
  }
}

// 输出构造函数
module.exports = MyPromise

